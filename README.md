This is my own custom prelude for Haskell. It is not intended to be of
interest for other users, but has a minimal dependency footprint (just
`base`), so should be an acceptable dependency for any project.

To use:

* Don't import the usual prelude (via `LANGUAGE NoImplicitPrelude`), or
  import it qualified.
* Add `import Zhp` to the top of your modules.
