module Zhp
    (
    -- * Renamed functions
      sequence
    , sequence_

    -- * Whole modules we re-export
    , module Control.Category
    , module Data.Bits
    , module Data.Int
    , module Data.Proxy
    , module Data.Word

    -- * Individual items re-exported as-is from parts of base.
    , (&&)
    , (&)
    , (<=<)
    , (=<<)
    , (>=>)
    , (^)
    , (^^)
    , (||)
    , (<$!>)
    , (<$>)
    , ($!)
    , getArgs
    , getProgName
    , getExecutablePath
    , lookupEnv
    , setEnv
    , unsetEnv
    , withArgs
    , withProgName
    , getEnvironment
    , ($)
    , all
    , Alternative(..)
    , and
    , any
    , appendFile
    , Applicative(..)
    , asTypeOf
    , asum
    , Bool(..)
    , Bounded(..)
    , break
    , BufferMode(..)
    , Char
    , concatMap
    , const
    , curry
    , cycle
    , catMaybes
    , fromMaybe
    , isJust
    , isNothing
    , mapMaybe
    , Double
    , drop
    , dropWhile
    , either
    , Either(..)
    , Enum(..)
    , Eq(..)
    , error
    , even
    , exitFailure
    , exitSuccess
    , exitWith
    , FilePath(..)
    , filter
    , flip
    , Float
    , Floating(..)
    , Foldable(foldMap, foldr, foldl', elem, sum, product) -- omit foldl
    , foldM_
    , foldM
    , for
    , for_
    , forever
    , Fractional(..)
    , fromIntegral
    , fst
    , Functor(..)
    , gcd
    , getChar
    , getContents
    , getLine
    , guard
    , Handle
    , hClose
    , hFileSize
    , hFlush
    , hGetBuffering
    , hGetChar
    , hGetContents
    , hGetLine
    , hIsEOF
    , hPrint
    , hPutChar
    , hPutStr
    , hPutStrLn
    , hSeek
    , hSetBinaryMode
    , hSetBuffering
    , hSetFileSize
    , Integral(..)
    , interact
    , IO(..)
    , IOError(..)
    , IOMode(..)
    , isEOF
    , IsString(..)
    , iterate
    , join
    , lcm
    , length
    , lines
    , map
    , maybe
    , Maybe(..)
    , Monad((>>=))
    , MonadIO(..)
    , Monoid(mempty, mconcat) -- mappend is just (<>) from semigroup.
    , not
    , notElem
    , null
    , Num(..)
    , odd
    , openBinaryFile
    , openBinaryTempFile
    , openTempFile
    , or
    , Ord(..)
    , Ordering(..)
    , otherwise
    , print
    , printf
    , putChar
    , putStr
    , putStrLn
    , Rational
    , Read(..)
    , readFile
    , Real(..)
    , RealFloat(..)
    , RealFrac(..)
    , realToFrac
    , repeat
    , replicate
    , replicateM
    , replicateM_
    , reverse
    , scanl
    , scanl1
    , scanr
    , scanr1
    , SeekMode(..)
    , Semigroup(..)
    , seq
    , Show(..)
    , snd
    , span
    , splitAt
    , stderr
    , stdin
    , stdout
    , String(..)
    , subtract
    , take
    , takeWhile
    , traverse
    , traverse_
    , uncurry
    , undefined
    , unless
    , unlines
    , until
    , unwords
    , unzip
    , unzip3
    , void
    , when
    , withBinaryFile
    , words
    , writeFile
    , zip
    , zip3
    , zipWith
    , zipWith3
    , isAlphaNum
    , isAscii
    , isControl
    , isDigit
    , isHexDigit
    , isLatin1
    , isLetter
    , isLower
    , isOctDigit
    , isPrint
    , isSpace
    , isUpper
    , toLower
    , toUpper
    ) where


-- TODO:
--
-- * consider not re-exporting 'many' and (<|>); these conflict with parsec.
-- * Data.Maybe

import Control.Applicative    (Alternative(..))
import Control.Category
import Control.Monad          hiding (sequence, sequence_)
import Control.Monad.IO.Class (MonadIO(..))
import Data.Bits
import Data.Char
    ( isAlphaNum
    , isAscii
    , isControl
    , isDigit
    , isHexDigit
    , isLatin1
    , isLetter
    , isLower
    , isOctDigit
    , isPrint
    , isSpace
    , isUpper
    , toLower
    , toUpper
    )
import Data.Foldable          hiding (sequence_)
import Data.Function          ((&))
import Data.Int
import Data.Maybe
    (catMaybes, fromMaybe, isJust, isNothing, mapMaybe)
import Data.Proxy
import Data.String            (IsString(..))
import Data.Traversable       hiding (sequence)
import Data.Word
import Prelude                hiding (id, sequence, sequence_, (.))
import System.Environment
import System.Exit
import System.IO
import Text.Printf            (printf)

-- | Alias for 'sequenceA_'
sequence_ :: (Foldable t, Applicative f) => t (f a) -> f ()
sequence_ = sequenceA_

-- | Alias for 'sequenceA'
sequence :: (Traversable t, Applicative f) => t (f a) -> f (t a)
sequence = sequenceA
